#!/bin/bash

set -xeu -o pipefail
mypath=`dirname $0`
SCRIPTS=$mypath

## Amoeba to consider (list from Arun Venkatesan):
# Balamuthia mandrillaris, Naegleria fowlerii, Acanthamoeba species.
## Acanthamoeba maybe from http://amoebadb.org/common/downloads/Current_Release/AcastellaniiNeff/fasta/data/AmoebaDB-4.2_AcastellaniiNeff_Genome.fasta

DIR="${1:-.}"
RES_DIR="${2:-.}"

#METAZOA=`printf "Metazoa/%s" Schistosoma_mansoni`
METAZOA=""
PROTISTS=`printf "Protists/%s " Leishmania_major Plasmodium_falciparum Toxoplasma_gondii Trypanosoma_brucei`

K_SPP="$METAZOA $PROTISTS"

RSYNC="rsync"
RSYNC_ARGS="--partial -a -i"

tolower() { echo $1 | tr '[:upper:]' '[:lower:]'; }

for K_SP in $K_SPP; do
    ## Extract kingdom and species from K_SPP
    Kingdom=`dirname $K_SP`
    kingdom="$(tolower $Kingdom)"

    Species=`basename $K_SP`
    species="$(tolower $Species)"
    Species_w_spaces="${Species//_/ }"

    ## Update FASTA header to contain >krakendb:taxid|<taxid>
    TAXID=`$SCRIPTS/get_sequences_from_ncbi "$Species_w_spaces" --just-taxid`
    [[ "$TAXID" != "" ]] || exit 1
    echo $Species_w_spaces : $TAXID

    DL_DIR="$DIR/$Kingdom"
    [[ -d "$DL_DIR" ]] || mkdir -vp "$DL_DIR"

    ## Download and unzip file
    rsync ${RSYNC_ARGS} rsync://ftp.ensemblgenomes.org/all/pub/current/$kingdom/fasta/$species/dna/*.dna_sm.genome.fa.gz "$DL_DIR"
    GZFILE=`find $DL_DIR -name "${Species}.*.dna_sm.genome.fa.gz" -maxdepth 1`;
    BN=`basename "$GZFILE" .gz`
    [[ -f "$GZFILE" ]] || exit 1

    MY_RES_DIR="$RES_DIR/$Kingdom"
    FILE=${MY_RES_DIR}/$BN
    [[ -d "$MY_RES_DIR" ]] || mkdir -vp "$MY_RES_DIR"
    gunzip -c "$GZFILE" |\
		$mypath/hardmask_and_write.pl $RES_DIR/Contaminants/masked-$BN - |\
		sed "s/^>/>kraken:taxid|$TAXID| /" > $FILE 
done

echo "Finished downloading $K_SPP genomes!"
