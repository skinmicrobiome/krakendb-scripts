#! /usr/bin/env perl
#
# hardmask_and_write.pl reads a FASTA from STDIN, and 
#  1) writes all lower-cased parts that are longer than 20bp to $ARGV[0],
#       appending to the file if it exists
#  2) substitutes these parts with a single N in the input sequence
#       and writes the results to $ARGV[1]
#
# Author fbreitwieser <fbreitwieser@igm3>
# Version 0.1
# Copyright (C) 2015 fbreitwieser <fbreitwieser@igm3>
# Modified On 2015-03-11 15:06
# Created  2015-03-11 12:03
#
use strict;
use warnings;
use File::Path;
use File::Basename;

my $seq = undef;
my $header_def = "kraken:taxid|81077 masked seq: ";
my $current_header = "default header";


my ($file_lowercased,$file_uppercased) = @ARGV;
die "Not two files given" unless defined $file_uppercased;

my $UC;
if (! -d dirname($file_lowercased)) { make_path(dirname($file_lowercased)); }
open(my $LC,">>",$file_lowercased) or die;
if ($file_uppercased eq "-") {
    open($UC, '>&', \*STDOUT) or die $!;
} else {
	if (! -d dirname($file_uppercased)) { make_path(dirname($file_uppercased)); }
    open($UC,">",$file_uppercased) or die $!;
}

my $cnth=1;
while (<STDIN>) {
    chomp;
    if (/^>(.*)/) {
        my $new_header=$1;
        search_seq($seq,$current_header) if (defined $seq);

        $current_header=$new_header;
        $seq="";
        $cnth=1;
    } elsif (defined $seq && length($seq) > 10000) {
        search_seq($seq,$current_header.$cnth++);
        $seq="";
    } else {
        s/[Nn]+/N/g;
        $seq .= $_;
    }
}
search_seq($seq,$current_header);

sub search_seq {
    my ($seq,$current_header) = @_;
    my $cnt = 1;
    # search through sequence to find lower-case letters
    while ($seq =~ s/([agct]+)/N/) {
        if (length($1) > 20) {
            print $LC ">$header_def",$current_header," ",$cnt++,"\n",$1,"\n";
        }
    }
    print $UC ">",$current_header;
    print $UC substr($seq,0,80,'') . "\n" while ($seq);
}

close($LC);
close($UC);
