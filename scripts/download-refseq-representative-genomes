#!/bin/bash

set -eu -o pipefail
PROTOZOA=`printf "protozoa/%s " Plasmodium_falciparum Leishmania_major Trypanosoma_brucei Trypanosoma_cruzi`
FUNGI=`printf "fungi/%s " Aspergillus_fumigatus Candida_albicans Candida_dubliniensis Candida_glabrata Cryptococcus_gattii Cryptococcus_neoformans Encephalitozoon_cuniculi Encephalitozoon_intestinalis`

ORGANISMS=""

USAGE="USAGE: `basename $0` [OPTIONS] DOWNLOAD_DIR DUSTED_DIR

OPTIONS:
-p      Download protists (${PROTOZOA// /, })
-f      Download fungi (${FUNGI// /, })
"

while getopts ":pf" opt; do
  case "${opt}" in
    p)
      echo "-p was triggered"
      ORGANISMS="$ORGANISMS $PROTOZOA"
      ;;
    f)
      echo "-f was triggered"
      ORGANISMS="$ORGANISMS $FUNGI"
      ;;
  esac
done
shift $((OPTIND-1))

[[ "$ORGANISMS" != "" ]] || ( echo "Set either -p or -f" && echo "$USAGE" && exit 1 )

DIR="${1:-_download}"
DUSTED_DIR="${2:-library}"

SCRIPT_DIR=`dirname $0`

RSYNC_ARGS=(--partial -a -i '--include=*genomic.fna.gz' -L  -Lk --no-motd --delete '--exclude=*' --delete-excluded)

for K_SP in $ORGANISMS; do
  kingdom=`dirname $K_SP`
  Species=`basename $K_SP`
  Species_w_spaces="${Species//_/ }"

  ## Update FASTA header to contain >krakendb:taxid|<taxid>
  TAXID=`$SCRIPT_DIR/get_sequences_from_ncbi "$Species_w_spaces" --just-taxid`

  echo processing $Species_w_spaces [ taxid $TAXID ]

  [[ -d $DIR/$K_SP ]] || mkdir -vp $DIR/$K_SP
  rsync "${RSYNC_ARGS[@]}" "ftp.ncbi.nih.gov::genomes/refseq/$K_SP/representative/*/" $DIR/$K_SP | tee $DIR/$K_SP.log

  DIR=$DIR DUSTED_DIR=$DUSTED_DIR UNPACK=true TAXID=$TAXID $SCRIPT_DIR/process-rsync-log $K_SP
done
