

# Protists
## Leishmania major

Assemblies: http://www.ncbi.nlm.nih.gov/assembly/organism/5664/latest
Reference:  http://www.ncbi.nlm.nih.gov/assembly/GCF_000002725.2

ASM272v2

Organism name:    Leishmania major strain Friedlin
Infraspecific name:    Strain: Friedlin
BioSample:    SAMEA3138173
Submitter:    Friedlin Consortium
Date:    2011/02/14
Assembly level:    Complete Genome
Genome representation:    full
RefSeq category:    representative genome
GenBank assembly accession:    GCA_000002725.2 (latest)
RefSeq assembly accession:    GCF_000002725.2 (latest)
RefSeq assembly and GenBank assembly identical:    yes

Total sequence length   32,855,089
Total assembly gap length   0
Total number of chromosomes and plasmids    36

## Plasmodium falciparum

Assemblies: http://www.ncbi.nlm.nih.gov/assembly/organism/5833/latest
Reference: http://www.ncbi.nlm.nih.gov/assembly/GCF_000002765.3

ASM276v1

Organism name:    Plasmodium falciparum 3D7
Isolate:    3D7
Submitter:    Plasmodium falciparum Genome Sequencing Consortium
Date:    2009/08/06
Assembly level:    Chromosome with gaps
Genome representation:    full
RefSeq category:    representative genome
GenBank assembly accession:    GCA_000002765.1 (latest)
RefSeq assembly accession:    GCF_000002765.3 (latest)
RefSeq assembly and GenBank assembly identical: no
       Only in RefSeq: chromosome MT.

Total sequence length   23,270,305
Total assembly gap length   641
Number of contigs   24
Contig N50  1,418,244
Total number of chromosomes and plasmids    15

## Toxoplasma gondii

Assemblies: http://www.ncbi.nlm.nih.gov/assembly/organism/5811/latest/
No reference
Taking http://www.ncbi.nlm.nih.gov/assembly/GCA_000006565.2/

TGA4

Organism name:    Toxoplasma gondii ME49
Infraspecific name:    Strain: ME49
BioSample:    SAMN00255192
Submitter:    J. Craig Venter Institute
Date:    2013/08/02
Assembly level:    Chromosome
Genome representation:    full
GenBank assembly accession:    GCA_000006565.2 (latest)
RefSeq assembly accession:    n/a
RefSeq assembly and GenBank assembly identical:    n/a
WGS Project:    ABPA02
Assembly method:    Celera Assembler v. 1.92
Genome coverage:    26.55x
Sequencing technology:    454 GS FLX Titanium; Sanger

Total sequence length   65,669,694
Total assembly gap length   205,452
Gaps between scaffolds  13
Number of scaffolds 2,279
Scaffold N50    4,973,582
Number of contigs   2,511
Contig N50  1,219,553
Total number of chromosomes and plasmids    15

## Trypanosoma brucei

Assemblies: http://www.ncbi.nlm.nih.gov/assembly/organism/5691/latest/
Reference: http://www.ncbi.nlm.nih.gov/assembly/GCF_000002445.1/

ASM244v1

Organism name:    Trypanosoma brucei brucei TREU927
Infraspecific name:    Strain: 927/4 GUTat10.1
Isolate:    927/4 GUTat10.1
BioSample:    SAMN02953625
Submitter:    Trypanosoma brucei consortium
Date:    2005/12/14
Assembly level:    Chromosome
Genome representation:    full
RefSeq category:    representative genome
GenBank assembly accession:    GCA_000002445.1 (latest)
RefSeq assembly accession:    GCF_000002445.1 (latest)
RefSeq assembly and GenBank assembly identical:    yes

Total sequence length   26,075,494
Total assembly gap length   3,590
Gaps between scaffolds  0
Number of scaffolds 12
Scaffold N50    2,481,190
Number of contigs   51
Contig N50  1,608,198
Total number of chromosomes and plasmids    11

## Trypanosoma cruzi

Assemblies: http://www.ncbi.nlm.nih.gov/assembly/organism/5693/latest/
Reference: http://www.ncbi.nlm.nih.gov/assembly/GCF_000209065.1/

ASM20906v1

Organism name:    Trypanosoma cruzi
Infraspecific name:    Strain: CL Brener
BioSample:    SAMN02953627
Submitter:    Trypanosoma cruzi consortium
Date:    2005/08/02
Assembly level:    Scaffold
Genome representation:    full
RefSeq category:    representative genome
GenBank assembly accession:    GCA_000209065.1 (latest)
RefSeq assembly accession:    GCF_000209065.1 (latest)
RefSeq assembly and GenBank assembly identical:    yes
WGS Project:    AAHK01

IDs: 271508 [UID] 271488 [GenBank] 271508 [RefSeq] 

Total sequence length   89,937,456
Total assembly gap length   325,100
Gaps between scaffolds  0
Number of scaffolds 29,495
Scaffold N50    88,624
Number of contigs   32,746
Contig N50  14,669
Total number of chromosomes and plasmids    0

# Amoeba
## Naegleria fowleri

Assemblies: http://www.ncbi.nlm.nih.gov/assembly/organism/5761/latest/
Reference: http://www.ncbi.nlm.nih.gov/assembly/GCA_000499105.1/

Naegleria fowleri 1.0

Organism name:    Naegleria fowleri
Infraspecific name:    Strain: ATCC 30863
BioSample:    SAMN02351365
Submitter:    Spiez Laboratory
Date:    2013/11/25
Assembly level:    Contig
Genome representation:    full
RefSeq category:    representative genome
GenBank assembly accession:    GCA_000499105.1 (latest)
RefSeq assembly accession:    n/a
RefSeq assembly and GenBank assembly identical:    n/a
WGS Project:    AWXF01
Assembly method:    CLC Genomic Workbench v. 6.5
Genome coverage:    70.0x
Sequencing technology:    Illumina HiSeq; 45

Total sequence length   27,791,290
Total assembly gap length   407,885
Number of contigs   1,729
Contig N50  38,128
Total number of chromosomes and plasmids    0

## Naegleria gruberi

Is a non-pathogenic amoeba similiar to N fowleri. Consider adding.

# Metazoa
## Schistosoma mansoni

Assemblies: http://www.ncbi.nlm.nih.gov/assembly/organism/6183/latest/
Reference: http://www.ncbi.nlm.nih.gov/assembly/GCA_000237925.2/

ASM23792v2

Organism name:    Schistosoma mansoni
Infraspecific name:    Strain: Puerto Rico
BioSample:    SAMEA2272516
Submitter:    Schistosoma Genome Network
Date:    2011/12/13
Assembly level:    Chromosome
Genome representation:    full
RefSeq category:    representative genome
GenBank assembly accession:    GCA_000237925.2 (latest)
RefSeq assembly accession:    n/a
RefSeq assembly and GenBank assembly identical:    n/a
WGS Project:    CABG01

Total sequence length   364,538,298
Total assembly gap length   1,450,291
Number of contigs   6,744
Contig N50  115,676
Total number of chromosomes and plasmids    9


