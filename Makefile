## DEFINE ON THE COMMAND LINE: DL DB
THIS_MAKEFILE_PATH:=$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
THIS_DIR:=$(shell cd $(dir $(THIS_MAKEFILE_PATH));pwd)
THIS_MAKEFILE:=$(notdir $(THIS_MAKEFILE_PATH))
SHELL:=/bin/bash

SCRIPTS_DIR=$(THIS_DIR)/scripts

DB="."
DL="_download"

KRAKEN_BUILD="--build"
FASTA=*.fna

LIB_DIR=$(DB)/library

define USAGE

Usage: 
  make [OPTION=value]* TARGET

Options:
	DB=$(DB)
	DL=$(DL)

Targets:
	help	this screen
	all 	make all of the targets below

	get-human-seq
	get-contaminant-seq
	get-ncbi-genomes

	build
  
endef
export USAGE

help:
	@echo "$$USAGE"

all: taxonomy contaminants bacteria viruses plasmids build-db
	
taxonomy:
	kraken-build --db $(DB) --download-taxonomy

human:
	kraken-build --db $(DB) --download-library human

contaminants: 
	[[ -d $(DB)/library/Contaminants ]] || mkdir -pv $(DB)/library/Contaminants
	# copy contaminant sequences to the library
	cp -v $(THIS_DIR)/contaminants/*.{fa,fna} $(DB)/library/Contaminants
	# download UniVec database to the library (modifies the headers to be picked up by Kraken)
	$(SCRIPTS_DIR)/download-univec-database $(DB)/library/Contaminants/UniVec.fa

bacteria:
	$(SCRIPTS_DIR)/download-ncbi-bacteria_viruses_plasmids-genomes -b $(DL) $(LIB_DIR)

viruses:
	$(SCRIPTS_DIR)/download-ncbi-bacteria_viruses_plasmids-genomes -v $(DL) $(LIB_DIR)

plasmids:
	$(SCRIPTS_DIR)/download-ncbi-bacteria_viruses_plasmids-genomes -p $(DL) $(LIB_DIR)

fungi:
	$(SCRIPTS_DIR)/download-refseq-representative-genomes -f $(DL) $(LIB_DIR)

protists:
	$(SCRIPTS_DIR)/download-refseq-representative-genomes -p $(DL) $(LIB_DIR)
	$(SCRIPTS_DIR)/download-further-ncbi-genomes $(DL) $(LIB_DIR)


build-db:
	kraken-build --db $(DB) --build --threads 8

rebuild-db:
	kraken-build --db $(DB) --rebuild --threads 8

set-lcas: 
	/data1/igm3/sw/packages/kraken-0.10.5-beta/install/set_lcas -d database.kdb -i database.idx \
		-n taxonomy/nodes.dmp -m seqid2taxid.map -x -F <( sed 's/^>/>VECTORSEQ /' $(FASTA) )

